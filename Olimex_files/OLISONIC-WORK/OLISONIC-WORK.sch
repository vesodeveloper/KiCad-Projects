EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "OLISONIC"
Date ""
Rev ""
Comp "OLIMEX LTD"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L OLIMEX_RCL:R R1
U 1 1 62DA7B14
P 1300 4000
F 0 "R1" H 1200 4200 50  0000 L CNN
F 1 "1.5k/0.25W" H 1100 4300 50  0000 L CNN
F 2 "OLIMEX_RLC-FP:R_0.25W_PTH" V 1391 4070 30  0001 L CNN
F 3 "" V 1300 4000 30  0000 C CNN
	1    1300 4000
	0    1    1    0   
$EndComp
$Comp
L OLIMEX_Diodes:LED LED1
U 1 1 62DA80EE
P 1300 4500
F 0 "LED1" H 1399 4423 50  0000 R CNN
F 1 "PWR LED/3mm/Green" H 1600 4300 50  0000 R CNN
F 2 "OLIMEX_LEDs-FP:LED-3mm-PTH-KA" V 4800 5200 60  0001 R CNN
F 3 "" H 1300 4500 60  0000 C CNN
	1    1300 4500
	0    -1   -1   0   
$EndComp
$Comp
L OLIMEX_Power:GND #PWR01
U 1 1 62DAC50F
P 1300 5000
F 0 "#PWR01" H 1300 4750 50  0001 C CNN
F 1 "GND" H 1305 4827 50  0000 C CNN
F 2 "" H 1300 5000 60  0000 C CNN
F 3 "" H 1300 5000 60  0000 C CNN
	1    1300 5000
	1    0    0    -1  
$EndComp
Wire Wire Line
	1300 4150 1300 4300
$Comp
L OLIMEX_RCL:R 22R/0.125W1
U 1 1 62DADAE9
P 2800 5600
F 0 "22R/0.125W1" H 3200 5700 50  0000 C CNN
F 1 "R5" H 2600 5600 50  0000 C CNN
F 2 "OLIMEX_RLC-FP:R_0.125W_PTH" H 2800 5530 30  0001 C CNN
F 3 "" V 2800 5600 30  0000 C CNN
	1    2800 5600
	1    0    0    -1  
$EndComp
$Comp
L OLIMEX_RCL:R R4
U 1 1 62DAE1EA
P 2800 5500
F 0 "R4" H 2500 5500 50  0000 C CNN
F 1 "22R/0.125W" H 3200 5400 50  0000 C CNN
F 2 "OLIMEX_RLC-FP:R_0.25W_PTH" H 2800 5430 30  0001 C CNN
F 3 "" V 2800 5500 30  0000 C CNN
	1    2800 5500
	1    0    0    -1  
$EndComp
$Comp
L OLIMEX_Power:GND #PWR02
U 1 1 62DB647B
P 1700 6100
F 0 "#PWR02" H 1700 5850 50  0001 C CNN
F 1 "GND" H 1705 5927 50  0000 C CNN
F 2 "" H 1700 6100 60  0000 C CNN
F 3 "" H 1700 6100 60  0000 C CNN
	1    1700 6100
	1    0    0    -1  
$EndComp
$Comp
L OLIMEX_Power:GND #PWR05
U 1 1 62DB6B92
P 2300 6100
F 0 "#PWR05" H 2300 5850 50  0001 C CNN
F 1 "GND" H 2305 5927 50  0000 C CNN
F 2 "" H 2300 6100 60  0000 C CNN
F 3 "" H 2300 6100 60  0000 C CNN
	1    2300 6100
	1    0    0    -1  
$EndComp
$Comp
L OLIMEX_Diodes:SMBJ6.0A D3
U 1 1 62DB7044
P 1700 5900
F 0 "D3" H 1500 5600 50  0000 R CNN
F 1 "NA(BZV55C3V6/SOD80C)" H 1800 5500 50  0000 R CNN
F 2 "OLIMEX_Diodes-FP:SOD80C-4148-KA" H 1730 6050 20  0001 C CNN
F 3 "" H 1700 5900 60  0000 C CNN
	1    1700 5900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1700 6100 1700 6000
Wire Wire Line
	1700 5800 1700 5600
Wire Wire Line
	1700 5600 1000 5600
$Comp
L OLIMEX_Diodes:SMBJ6.0A D4
U 1 1 62DB7C85
P 2300 5900
F 0 "D4" H 2100 5700 50  0000 R CNN
F 1 "NA(BZV55C3V6/SOD80C)" H 2400 5500 50  0000 R CNN
F 2 "OLIMEX_Diodes-FP:SOD80C-4148-KA" H 2330 6050 20  0001 C CNN
F 3 "" H 2300 5900 60  0000 C CNN
	1    2300 5900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1000 5500 1700 5500
Wire Wire Line
	7100 4200 5800 4200
Wire Wire Line
	1700 5600 2650 5600
Connection ~ 1700 5600
Wire Wire Line
	2300 6100 2300 6000
Wire Wire Line
	2300 5800 2300 5500
Connection ~ 2300 5500
Wire Wire Line
	2300 5500 2650 5500
$Comp
L OLIMEX_RCL:R R2
U 1 1 62DBD93B
P 1700 4700
F 0 "R2" H 1700 4900 50  0000 L CNN
F 1 "1.5k/0.25W" H 1500 5000 50  0000 L CNN
F 2 "OLIMEX_RLC-FP:R_0.125W_PTH" V 1791 4770 30  0001 L CNN
F 3 "" V 1700 4700 30  0000 C CNN
	1    1700 4700
	0    1    1    0   
$EndComp
Wire Wire Line
	1700 4850 1700 5500
Connection ~ 1700 5500
Wire Wire Line
	1700 5500 2300 5500
$Comp
L OLIMEX_Buttons:T1107A(6x3,8x2,5MM) SW1
U 1 1 62DBFDA9
P 7300 5300
F 0 "SW1" V 7353 5255 60  0000 R CNN
F 1 "TSR-1(T1101GP/6x3x4.3)" V 7247 5255 60  0000 R CNN
F 2 "OLIMEX_Switches-FP:IT1280A" H 7299 5338 60  0001 C CNN
F 3 "" H 7299 5338 60  0000 C CNN
	1    7300 5300
	0    -1   -1   0   
$EndComp
$Comp
L OLIMEX_Power:GND #PWR06
U 1 1 62DC1C75
P 7300 5700
F 0 "#PWR06" H 7300 5450 50  0001 C CNN
F 1 "GND" H 7305 5527 50  0000 C CNN
F 2 "" H 7300 5700 60  0000 C CNN
F 3 "" H 7300 5700 60  0000 C CNN
	1    7300 5700
	1    0    0    -1  
$EndComp
$Comp
L OLIMEX_RCL:C C2
U 1 1 62DC5F47
P 2200 4100
F 0 "C2" H 2292 4146 50  0000 L CNN
F 1 "100nF/2010" H 2292 4055 50  0000 L CNN
F 2 "TEMPORARY:1uF_capacitor_fpt" H 2200 4100 60  0001 C CNN
F 3 "" H 2200 4100 60  0000 C CNN
	1    2200 4100
	1    0    0    -1  
$EndComp
$Comp
L OLIMEX_Power:GND #PWR04
U 1 1 62DC567C
P 2200 5000
F 0 "#PWR04" H 2200 4750 50  0001 C CNN
F 1 "GND" H 2205 4827 50  0000 C CNN
F 2 "" H 2200 5000 60  0000 C CNN
F 3 "" H 2200 5000 60  0000 C CNN
	1    2200 5000
	1    0    0    -1  
$EndComp
Wire Wire Line
	2200 4200 2200 4400
Connection ~ 2200 4400
Wire Wire Line
	6800 4300 5800 4300
$Comp
L OLIMEX_Devices:Buzzer_2015 U4
U 1 1 62DD18BF
P 9800 2400
F 0 "U4" V 9754 2828 50  0000 L CNN
F 1 "Buzzer_2015" V 9845 2828 50  0000 L CNN
F 2 "TEMPORARY:ULTRASONIC_HORISONTAL" H 9800 2400 50  0001 C CNN
F 3 "" H 9800 2400 50  0001 C CNN
	1    9800 2400
	-1   0    0    1   
$EndComp
$Comp
L OLIMEX_Devices:Buzzer_2015 U5
U 1 1 62DD1D6E
P 10000 3900
F 0 "U5" V 9954 4328 50  0000 L CNN
F 1 "Buzzer_2015" V 10045 4328 50  0000 L CNN
F 2 "TEMPORARY:ULTRASONIC_HORISONTAL" H 10000 3900 50  0001 C CNN
F 3 "" H 10000 3900 50  0001 C CNN
	1    10000 3900
	1    0    0    -1  
$EndComp
Text GLabel 9900 1900 1    50   Input ~ 0
spout1
Wire Wire Line
	9900 1900 9900 2100
Text GLabel 9700 1900 1    50   Input ~ 0
spout2
Wire Wire Line
	9700 1900 9700 2100
Text GLabel 10100 4500 3    50   Input ~ 0
spout2
Wire Wire Line
	10100 4500 10100 4200
Text GLabel 9900 4500 3    50   Input ~ 0
spout1
Wire Wire Line
	9900 4500 9900 4200
Text GLabel 2700 1800 0    50   Input ~ 0
PB0_OC0A
Wire Wire Line
	3400 1800 2700 1800
Connection ~ 2200 2800
Wire Wire Line
	1900 2800 2200 2800
Wire Wire Line
	1900 2800 1700 2800
Connection ~ 1900 2800
Wire Wire Line
	1900 3500 1900 3700
Wire Wire Line
	1900 2800 1900 3300
$Comp
L OLIMEX_RCL:CP C1
U 1 1 62DC33B5
P 1900 3400
F 0 "C1" V 1988 3446 50  0000 L CNN
F 1 "10uF/16V/105C" V 2100 3200 50  0000 L CNN
F 2 "OLIMEX_RLC-FP:CPOL-RM2mm_5x11mm_PTH" H 1900 3400 60  0001 C CNN
F 3 "" H 1900 3400 60  0000 C CNN
	1    1900 3400
	1    0    0    -1  
$EndComp
$Comp
L OLIMEX_Power:GND #PWR03
U 1 1 62DC2679
P 1900 3700
F 0 "#PWR03" H 1900 3450 50  0001 C CNN
F 1 "GND" H 1905 3527 50  0000 C CNN
F 2 "" H 1900 3700 60  0000 C CNN
F 3 "" H 1900 3700 60  0000 C CNN
	1    1900 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	1700 2800 1300 2800
Connection ~ 1700 2800
Wire Wire Line
	1700 2800 1700 4550
Connection ~ 1300 2800
Wire Wire Line
	1000 2800 1300 2800
Wire Wire Line
	1300 2800 1300 3850
$Comp
L OLIMEX_RCL:R R6
U 1 1 62DABC53
P 2500 2800
F 0 "R6" H 2500 3007 50  0000 C CNN
F 1 "NA" H 2500 2916 50  0000 C CNN
F 2 "OLIMEX_RLC-FP:R_0.25W_PTH" H 2500 2730 30  0001 C CNN
F 3 "" V 2500 2800 30  0000 C CNN
	1    2500 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	7100 4200 7100 5500
Wire Wire Line
	2950 5500 7100 5500
Wire Wire Line
	6800 4300 6800 5600
Wire Wire Line
	2950 5600 6800 5600
Wire Wire Line
	2200 2800 2350 2800
Text GLabel 6000 3900 2    50   Input ~ 0
PB0_OC0A
Text GLabel 4500 1900 2    50   Input ~ 0
spout2
Wire Wire Line
	1300 4700 1300 5000
Wire Wire Line
	2200 2800 2200 3900
Connection ~ 2200 3900
Wire Wire Line
	2200 3900 2200 4000
Wire Wire Line
	7300 5600 7300 5700
Wire Wire Line
	4500 1900 4400 1900
Wire Wire Line
	2200 4400 3200 4400
Wire Wire Line
	4600 1300 4600 1200
$Comp
L OLIMEX_Power:GND #PWR08
U 1 1 62E11712
P 5900 1600
F 0 "#PWR08" H 5900 1350 50  0001 C CNN
F 1 "GND" H 5905 1427 50  0000 C CNN
F 2 "" H 5900 1600 60  0000 C CNN
F 3 "" H 5900 1600 60  0000 C CNN
	1    5900 1600
	1    0    0    -1  
$EndComp
Wire Wire Line
	2200 4400 2200 5000
Wire Wire Line
	7300 4400 7300 5000
$Comp
L OLIMEX_Power:GND #PWR0105
U 1 1 62E13F58
P 4700 2100
F 0 "#PWR0105" H 4700 1850 50  0001 C CNN
F 1 "GND" V 4705 1972 50  0000 R CNN
F 2 "" H 4700 2100 60  0000 C CNN
F 3 "" H 4700 2100 60  0000 C CNN
	1    4700 2100
	0    -1   -1   0   
$EndComp
$Comp
L OLIMEX_Power:GND #PWR0109
U 1 1 62EDB802
P 1300 6100
F 0 "#PWR0109" H 1300 5850 50  0001 C CNN
F 1 "GND" H 1305 5927 50  0000 C CNN
F 2 "" H 1300 6100 60  0000 C CNN
F 3 "" H 1300 6100 60  0000 C CNN
	1    1300 6100
	1    0    0    -1  
$EndComp
Wire Wire Line
	4400 1300 4500 1300
Wire Wire Line
	4500 1300 4500 700 
Wire Wire Line
	4500 700  4400 700 
Connection ~ 4500 1300
Wire Wire Line
	4500 1300 4600 1300
$Comp
L OLIMEX_Power:GND #PWR0106
U 1 1 62EF7698
P 4000 700
F 0 "#PWR0106" H 4000 450 50  0001 C CNN
F 1 "GND" H 4005 527 50  0000 C CNN
F 2 "" H 4000 700 60  0000 C CNN
F 3 "" H 4000 700 60  0000 C CNN
	1    4000 700 
	1    0    0    -1  
$EndComp
$Comp
L OLIMEX_Connectors:USB_A_VERTICAL USB_DEV1
U 1 1 62E15E14
P 800 5600
F 0 "USB_DEV1" H 743 6067 50  0000 C CNN
F 1 "USB-B" H 743 5976 50  0000 C CNN
F 2 "TEMPORARY:USB-B" H 830 5750 20  0001 C CNN
F 3 "" H 800 5600 60  0000 C CNN
	1    800  5600
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1000 5700 1300 5700
Wire Wire Line
	1300 5700 1300 5800
Wire Wire Line
	1000 5800 1300 5800
Connection ~ 1300 5800
Wire Wire Line
	1300 5800 1300 6100
Wire Wire Line
	5800 4000 6800 4000
$Comp
L OLIMEX_IC:ST3232B(C)DR(SO-16) U3
U 1 1 62E2C929
P 3900 1700
F 0 "U3" H 3900 2365 50  0000 C CNN
F 1 "MAX232EN" H 3900 2274 50  0000 C CNN
F 2 "TEMPORARY:MAX232" H 3500 1050 50  0001 L CNN
F 3 "https://www.st.com/resource/en/datasheet/st3232c.pdf" H 4350 2000 50  0001 L CNN
F 4 "3 to 5.5 V, low-power, up to 400 kbs RS-232 drivers and receivers" H 2400 850 50  0001 L CNN "Description"
F 5 "STMicroelectronics" H 3500 950 50  0001 L CNN "Manufacturer_Name"
	1    3900 1700
	1    0    0    -1  
$EndComp
Wire Wire Line
	3400 1200 3400 1300
Wire Wire Line
	2800 1200 3400 1200
Wire Wire Line
	4400 1800 4600 1800
Text GLabel 4600 1800 2    50   Input ~ 0
spout1
Wire Wire Line
	4400 2100 4700 2100
Wire Wire Line
	3400 1900 2700 1900
Text GLabel 2700 1900 0    50   Input ~ 0
PB1_OCOB
Text GLabel 6800 3800 1    50   Input ~ 0
PB1_OCOB
Wire Wire Line
	6800 3800 6800 4000
$Comp
L OLIMEX_Power:+5V #PWR0101
U 1 1 62E99FA9
P 4600 1200
F 0 "#PWR0101" H 4600 1050 50  0001 C CNN
F 1 "+5V" H 4615 1373 50  0000 C CNN
F 2 "" H 4600 1200 60  0000 C CNN
F 3 "" H 4600 1200 60  0000 C CNN
	1    4600 1200
	1    0    0    -1  
$EndComp
$Comp
L OLIMEX_Power:+5V #PWR0104
U 1 1 62E9A88D
P 800 4800
F 0 "#PWR0104" H 800 4650 50  0001 C CNN
F 1 "+5V" H 815 4973 50  0000 C CNN
F 2 "" H 800 4800 60  0000 C CNN
F 3 "" H 800 4800 60  0000 C CNN
	1    800  4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	1000 3850 1000 2800
$Comp
L OLIMEX_Diodes:D D2
U 1 1 62DB3976
P 1000 4500
F 0 "D2" H 1000 4600 50  0000 L CNN
F 1 "1N4148/DO35" H 900 4700 50  0000 L CNN
F 2 "TEMPORARY:diode" H 1000 4500 60  0001 C CNN
F 3 "" H 1000 4500 60  0000 C CNN
	1    1000 4500
	0    1    1    0   
$EndComp
$Comp
L OLIMEX_Diodes:D D1
U 1 1 62DB4845
P 1000 4000
F 0 "D1" H 1000 3900 50  0000 L CNN
F 1 "1N4148/DO35" H 700 4100 50  0000 L CNN
F 2 "TEMPORARY:diode" H 1000 4000 60  0001 C CNN
F 3 "" H 1000 4000 60  0000 C CNN
	1    1000 4000
	0    1    1    0   
$EndComp
Wire Wire Line
	1000 4350 1000 4150
Wire Wire Line
	1000 5400 1000 5000
Wire Wire Line
	1000 5000 800  5000
Wire Wire Line
	800  5000 800  4800
Connection ~ 1000 5000
Wire Wire Line
	1000 5000 1000 4650
$Comp
L OLIMEX_RCL:CP C4
U 1 1 62EB90AF
P 2800 1300
F 0 "C4" V 2525 1250 50  0000 L CNN
F 1 "4.7uF/50V" V 2625 1100 50  0000 L CNN
F 2 "OLIMEX_RLC-FP:CPOL-RM2mm_5x11mm_PTH" H 2800 1300 60  0001 C CNN
F 3 "" H 2800 1300 60  0000 C CNN
	1    2800 1300
	1    0    0    -1  
$EndComp
Wire Wire Line
	3000 1500 3400 1500
Wire Wire Line
	2800 1400 3400 1400
$Comp
L OLIMEX_RCL:CP C5
U 1 1 62EBAFED
P 3000 1600
F 0 "C5" V 2750 1475 50  0000 L CNN
F 1 "4.7uF/50V" V 2875 1325 50  0000 L CNN
F 2 "OLIMEX_RLC-FP:CPOL-RM2mm_5x11mm_PTH" H 3000 1600 60  0001 C CNN
F 3 "" H 3000 1600 60  0000 C CNN
	1    3000 1600
	1    0    0    -1  
$EndComp
Wire Wire Line
	3400 1600 3100 1600
Wire Wire Line
	3100 1600 3100 1700
Wire Wire Line
	3100 1700 3000 1700
$Comp
L OLIMEX_RCL:CP C6
U 1 1 62EE540D
P 5300 1400
F 0 "C6" V 5212 1354 50  0000 R CNN
F 1 "4.7uF/50V" V 5100 1500 50  0000 R CNN
F 2 "OLIMEX_RLC-FP:CPOL-RM2mm_5x11mm_PTH" H 5300 1400 60  0001 C CNN
F 3 "" H 5300 1400 60  0000 C CNN
	1    5300 1400
	-1   0    0    1   
$EndComp
$Comp
L OLIMEX_Power:+5V #PWR09
U 1 1 62EE9DD4
P 5300 1200
F 0 "#PWR09" H 5300 1050 50  0001 C CNN
F 1 "+5V" H 5315 1373 50  0000 C CNN
F 2 "" H 5300 1200 60  0000 C CNN
F 3 "" H 5300 1200 60  0000 C CNN
	1    5300 1200
	1    0    0    -1  
$EndComp
Wire Wire Line
	4400 1500 5300 1500
Wire Wire Line
	5300 1300 5300 1200
$Comp
L OLIMEX_RCL:CP C3
U 1 1 62EF395A
P 5700 1600
F 0 "C3" V 5475 1600 50  0000 C CNN
F 1 "4.7uF/50V" V 5600 1700 50  0000 C CNN
F 2 "OLIMEX_RLC-FP:CPOL-RM2mm_5x11mm_PTH" H 5700 1600 60  0001 C CNN
F 3 "" H 5700 1600 60  0000 C CNN
	1    5700 1600
	0    1    1    0   
$EndComp
Wire Wire Line
	4400 1600 5600 1600
Wire Wire Line
	5800 1600 5900 1600
$Comp
L OLIMEX_RCL:CP C7
U 1 1 62EFB8B0
P 4300 700
F 0 "C7" V 4525 700 50  0000 C CNN
F 1 "4.7uF/50V" V 4434 700 50  0000 C CNN
F 2 "OLIMEX_RLC-FP:CPOL-RM2mm_5x11mm_PTH" H 4300 700 60  0001 C CNN
F 3 "" H 4300 700 60  0000 C CNN
	1    4300 700 
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4000 700  4200 700 
Wire Wire Line
	4400 1400 5000 1400
$Comp
L OLIMEX_Power:GND #PWR0107
U 1 1 62EB5FE0
P 5000 1400
F 0 "#PWR0107" H 5000 1150 50  0001 C CNN
F 1 "GND" H 5005 1227 50  0000 C CNN
F 2 "" H 5000 1400 60  0000 C CNN
F 3 "" H 5000 1400 60  0000 C CNN
	1    5000 1400
	0    -1   -1   0   
$EndComp
$Comp
L OLIMEX_IC:ATTINY85-20PU U1
U 1 1 62E3C6F2
P 4500 4100
F 0 "U1" H 4500 4565 50  0000 C CNN
F 1 "ATtiny85-20PU+(DIL-8)" H 4500 4474 50  0000 C CNN
F 2 "OLIMEX_IC-FP:PDIP-8" H 4500 3500 50  0001 C CNN
F 3 "" H 5800 4000 50  0001 C CNN
	1    4500 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	2200 3900 3200 3900
Wire Wire Line
	6600 4400 7300 4400
Wire Wire Line
	5800 4400 6600 4400
Connection ~ 6600 4400
Wire Wire Line
	2650 2800 6600 2800
Wire Wire Line
	6600 2800 6600 4400
Wire Wire Line
	5800 3900 6000 3900
Wire Wire Line
	7800 4400 7800 4300
Wire Wire Line
	7800 4800 7800 4900
$Comp
L OLIMEX_Power:GND #PWR0103
U 1 1 62E27DBD
P 7800 4900
F 0 "#PWR0103" H 7800 4650 50  0001 C CNN
F 1 "GND" H 7805 4727 50  0000 C CNN
F 2 "" H 7800 4900 60  0000 C CNN
F 3 "" H 7800 4900 60  0000 C CNN
	1    7800 4900
	1    0    0    -1  
$EndComp
$Comp
L OLIMEX_Power:VCC #PWR0102
U 1 1 62E27992
P 7800 4300
F 0 "#PWR0102" H 7800 4150 50  0001 C CNN
F 1 "VCC" H 7817 4473 50  0000 C CNN
F 2 "" H 7800 4300 60  0000 C CNN
F 3 "" H 7800 4300 60  0000 C CNN
	1    7800 4300
	1    0    0    -1  
$EndComp
$Comp
L OLIMEX_Devices:Potentiometer R5
U 1 1 62DCEED4
P 7800 4600
F 0 "R5" H 7880 4653 60  0000 L CNN
F 1 "Potentiometer" H 7880 4547 60  0000 L CNN
F 2 "TEMPORARY:potentiometer_fpr" H 7800 4575 200 0001 C CNN
F 3 "" H 7800 4575 200 0001 C CNN
	1    7800 4600
	1    0    0    -1  
$EndComp
Connection ~ 6800 4000
$Comp
L OLIMEX_RCL:R R7
U 1 1 62DCC3BE
P 8800 4300
F 0 "R7" V 8724 4370 50  0000 L CNN
F 1 "1.5k/0.25W" V 8815 4370 50  0000 L CNN
F 2 "OLIMEX_RLC-FP:R_0.25W_PTH" V 8891 4370 30  0001 L CNN
F 3 "" V 8800 4300 30  0000 C CNN
	1    8800 4300
	0    1    1    0   
$EndComp
$Comp
L OLIMEX_Power:GND #PWR07
U 1 1 62DCC8CA
P 8800 5200
F 0 "#PWR07" H 8800 4950 50  0001 C CNN
F 1 "GND" H 8805 5027 50  0000 C CNN
F 2 "" H 8800 5200 60  0000 C CNN
F 3 "" H 8800 5200 60  0000 C CNN
	1    8800 5200
	1    0    0    -1  
$EndComp
$Comp
L OLIMEX_Diodes:LED LED2
U 1 1 62DCCD1E
P 8800 4800
F 0 "LED2" V 8899 4723 50  0000 R CNN
F 1 "LED/3mm/Red" V 8808 4723 50  0000 R CNN
F 2 "OLIMEX_LEDs-FP:LED-3mm-PTH-KA" V 8709 4723 60  0001 R CNN
F 3 "" H 8800 4800 60  0000 C CNN
	1    8800 4800
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6800 4000 8800 4000
Wire Wire Line
	5800 4100 7400 4100
Wire Wire Line
	8800 4000 8800 4150
Wire Wire Line
	8800 4450 8800 4600
Wire Wire Line
	8800 5000 8800 5200
Wire Wire Line
	7400 4600 7600 4600
Wire Wire Line
	7400 4100 7400 4600
$EndSCHEMATC
